package edu.vinnicyus.banco_de_questoes.view;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import edu.vinnicyus.banco_de_questoes.ProgressViewStartEnd;
import edu.vinnicyus.banco_de_questoes.R;
import edu.vinnicyus.banco_de_questoes.models.Usuario;
import edu.vinnicyus.banco_de_questoes.request.HttpGlobalRetrofit;
import edu.vinnicyus.banco_de_questoes.request.api.UsuarioInterface;
import edu.vinnicyus.banco_de_questoes.request.deserializer.UsuarioDes;
import edu.vinnicyus.banco_de_questoes.request.models.request.UsuarioModel;
import edu.vinnicyus.banco_de_questoes.request.models.response.ErrorsResponseRes;
import edu.vinnicyus.banco_de_questoes.validator.ValidatorCodesResponse;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;

public class UserFragment extends Fragment {

    private EditText tvNome;
    private EditText tvEmail;
    private EditText tvCPF;
    private EditText tvMatricula;
    private EditText tvTelefone;

    private Realm realm;
    private Usuario usuario;

    private TextView btn_edit;
    private View progress;
    private View form;

    private ProgressViewStartEnd pv;
    private View falha;
    private TitleListener titleListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user, container, false);

        titleListener.setTitleActionBar("Banco de Questões");

        realm = Realm.getDefaultInstance();
        falha = view.findViewById(R.id.falha);

        btn_edit = view.findViewById(R.id.edit_usuario);
        progress = view.findViewById(R.id.progress_user);
        form = view.findViewById(R.id.form);

        tvNome = view.findViewById(R.id.nome_text);
        tvEmail = view.findViewById(R.id.email_text);
        tvTelefone = view.findViewById(R.id.telefone_text);
        tvMatricula = view.findViewById(R.id.matricula_text);
        tvCPF = view.findViewById(R.id.cpf_text);

        tvNome.setEnabled(false);
        tvEmail.setEnabled(false);
        tvTelefone.setEnabled(false);
        tvMatricula.setEnabled(false);
        tvCPF.setEnabled(false);

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(btn_edit.getText().equals("Salvar")) {
                    validation();
                }else {
                    btn_edit.setText("Salvar");

                    tvNome.setEnabled(true);
                    tvEmail.setEnabled(true);
                    tvTelefone.setEnabled(true);
                    tvCPF.setEnabled(true);
                }
            }
        });

        falha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onStart();
                falha.setVisibility(View.GONE);
            }
        });
        pv = new ProgressViewStartEnd(form,progress,getResources());

        return view;
    }
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity a;

        if (context instanceof Activity){
            a = (Activity) context;
            titleListener = (TitleListener) a;
        }

    }
    @Override
    public void onStart() {
        super.onStart();
        pv.show(true);
        Gson gson  = new GsonBuilder().registerTypeAdapter( Usuario.class, new UsuarioDes()).create();

        HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit(getContext());

        UsuarioInterface tokenInterface = retrofit.getRetrofit().create(UsuarioInterface.class);

        Call<Usuario> call = tokenInterface.getUser();
        call.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, retrofit2.Response<Usuario> response) {
                ValidatorCodesResponse<Usuario, Usuario.ErrorsUser> vc = new ValidatorCodesResponse<>(response, Usuario.ErrorsUser.class);
                vc.execute();
                if(vc.isStatus() == 1) {
                    Realm realm2 = Realm.getDefaultInstance();
                    tvNome.setText(response.body().getName());
                    tvEmail.setText(response.body().getEmail());
                    tvTelefone.setText(response.body().getTelefone());
                    tvMatricula.setText(response.body().getMatricula_prof());
                    tvCPF.setText(response.body().getCpf());

                    if(Usuario.getUser() == null){
                        realm2.beginTransaction();
                        Usuario usuario = realm2.createObject(Usuario.class);
                        usuario.setIdenti(1);
                        usuario.setValues(response.body());
                        realm2.commitTransaction();
                    }else {
                        realm2.beginTransaction();
                        Usuario usuario = Usuario.getUser();
                        usuario.setValues(response.body());
                        realm2.commitTransaction();
                    }
                    realm2.close();
                }
                pv.show(false);
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                progress.setVisibility(View.GONE);
                form.setVisibility(View.GONE);
                falha.setVisibility(View.VISIBLE);
            }
        });
    }

    private void validation(){
        boolean cancel = false;

        if (TextUtils.isEmpty(tvEmail.getText().toString())) {
            tvEmail.setError(getString(R.string.error_field_required));
            cancel = true;
        } else if (!isEmailValid(tvEmail.getText().toString())) {
            tvEmail.setError(getString(R.string.error_invalid_email));
            cancel = true;
        }

        if (TextUtils.isEmpty(tvNome.getText().toString())) {
            tvNome.setError(getString(R.string.error_field_required));
            cancel = true;
        }

        if (TextUtils.isEmpty(tvTelefone.getText().toString())) {
            tvTelefone.setError(getString(R.string.error_field_required));
            cancel = true;
        }

        if (TextUtils.isEmpty(tvCPF.getText().toString())) {
            tvCPF.setError(getString(R.string.error_field_required));
            cancel = true;
        }

        if(!cancel){

            UsuarioModel um = new UsuarioModel();
            um.setName(tvNome.getText().toString());
            um.setEmail(tvEmail.getText().toString());
            um.setTelefone(tvTelefone.getText().toString());
            um.setMatricula_prof(tvMatricula.getText().toString());
            um.setCpf(tvCPF.getText().toString());
            userUpdate(um);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    public void userUpdate(UsuarioModel um){

        pv.show(true);
        Realm realm = Realm.getDefaultInstance();
        Gson gson  = new GsonBuilder().registerTypeAdapter( Usuario.class, new UsuarioDes()).create();

        HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit(getContext());

        UsuarioInterface usuarioInterface = retrofit.getRetrofit().create(UsuarioInterface.class);
        Usuario u = Usuario.getUser();
        Call<Usuario> call = usuarioInterface.updateUser(""+u.getId(), um);
        call.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, retrofit2.Response<Usuario> response) {
                ValidatorCodesResponse<Usuario, Usuario.ErrorsUser> vc = new ValidatorCodesResponse<>(response, Usuario.ErrorsUser.class);
                vc.execute();
                if (vc.isStatus() == 1){
                    btn_edit.setText("Editar");

                    tvNome.setEnabled(false);
                    tvEmail.setEnabled(false);
                    tvTelefone.setEnabled(false);
                    tvCPF.setEnabled(false);

                    Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    Usuario u = Usuario.getUser();
                    u.setCpf(vc.getValue().getCpf());
                    u.setEmail(vc.getValue().getEmail());
                    u.setName(vc.getValue().getName());
                    u.setTelefone(vc.getValue().getTelefone());
                    realm.commitTransaction();
                    realm.close();

                    Toast.makeText(getContext(), "Atualizados!", Toast.LENGTH_SHORT).show();
                }else if(vc.isStatus() == 0){
                    tvNome.setError( vc.getErrors().getErrors().getName() != null ? vc.getErrors().getErrors().getName().get(0).toString() : null);
                    tvEmail.setError( vc.getErrors().getErrors().getEmail() != null ? vc.getErrors().getErrors().getEmail().get(0).toString() : null);
                    tvCPF.setError( vc.getErrors().getErrors().getCpf() != null ? vc.getErrors().getErrors().getCpf().get(0).toString() : null);
                    tvTelefone.setError( vc.getErrors().getErrors().getTelefone() != null ? vc.getErrors().getErrors().getTelefone().get(0).toString() : null);
                }else{
                    Toast.makeText(getContext(), "Error de Conexão!", Toast.LENGTH_SHORT).show();
                }
                pv.show(false);
            }
            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                Toast.makeText(getContext(), "Contate o Administrador!", Toast.LENGTH_SHORT).show();
            }
        });
        realm.close();
    }
}

