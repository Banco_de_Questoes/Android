package edu.vinnicyus.banco_de_questoes.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.security.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import edu.vinnicyus.banco_de_questoes.LoginActivity;
import edu.vinnicyus.banco_de_questoes.R;
import edu.vinnicyus.banco_de_questoes.models.Token;
import edu.vinnicyus.banco_de_questoes.request.HttpGlobalRetrofit;
import edu.vinnicyus.banco_de_questoes.request.api.QuestaoInterface;
import edu.vinnicyus.banco_de_questoes.request.deserializer.GetHistoricoDes;
import edu.vinnicyus.banco_de_questoes.request.models.response.GetHistoricoRes;
import edu.vinnicyus.banco_de_questoes.request.models.response.QuestaoRes;
import edu.vinnicyus.banco_de_questoes.view.Questao.AprovarQuestoesActivity;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;

public class HomeFragment extends Fragment {

    private TitleListener titleListener;
    private GraphView graphView;
    private GraphView graphView2;

    private View falha;
    private View progress;
    private View form;

    private TableLayout tl;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        setHasOptionsMenu(true);

        falha = view.findViewById(R.id.falha);
        form = view.findViewById(R.id.form);
        progress = view.findViewById(R.id.progress_user);
        tl = view.findViewById(R.id.tl_detalhes);

        titleListener.setTitleActionBar("Home");
        graphView = view.findViewById(R.id.graph);
        graphView2 = view.findViewById(R.id.graph2);

        Button btn_avaliar = view.findViewById(R.id.btn_avaliar);
        btn_avaliar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), AprovarQuestoesActivity.class));
            }
        });

        falha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onStart();
                falha.setVisibility(View.GONE);
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        form.setVisibility(View.GONE);
        progress.setVisibility(View.VISIBLE);
        getDados();
    }

    private void setGraficos(ArrayList<GetHistoricoRes.Historico> historicoRes){

        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(new DataPoint[] {
                new DataPoint(0, historicoRes.get(2).getQuestoes_elaboradas()),
                new DataPoint(1, historicoRes.get(1).getQuestoes_elaboradas()),
                new DataPoint(2, historicoRes.get(0).getQuestoes_elaboradas()),
        });
        series.setAnimated(true);
        series.setSpacing(20);
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graphView);
        staticLabelsFormatter.setHorizontalLabels(new String[] {historicoRes.get(2).getMes(),historicoRes.get(1).getMes(),historicoRes.get(0).getMes()});
        graphView.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        graphView.addSeries(series);

        BarGraphSeries<DataPoint> series2 = new BarGraphSeries<>(new DataPoint[] {
                new DataPoint(0, historicoRes.get(2).getQuestoes_aprovadas()),
                new DataPoint(1, historicoRes.get(1).getQuestoes_aprovadas()),
                new DataPoint(2, historicoRes.get(0).getQuestoes_aprovadas()),
        });
        series2.setAnimated(true);
        series2.setSpacing(20);
        StaticLabelsFormatter staticLabelsFormatter2 = new StaticLabelsFormatter(graphView2);
        staticLabelsFormatter2.setHorizontalLabels(new String[] {historicoRes.get(2).getMes(),historicoRes.get(1).getMes(),historicoRes.get(0).getMes()});
        graphView2.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter2);
        graphView2.addSeries(series2);


    }

    private void setListQuestao(ArrayList<QuestaoRes> questao) throws ParseException {
        if(questao.size() == 0){
            TableRow tr = new TableRow(getContext());
            TextView data = new TextView(getContext());
            data.setText("Nenhum resultado encontrado!");
            tr.addView(data);
            tl.addView(tr);
        }else {
            for (int i = 0; i < questao.size(); i++) {

                LinearLayout.LayoutParams layout = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                layout.setMargins(0, getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin), 0, 0);
                layout.setMarginStart(20);
                TableRow tr = new TableRow(getContext());
                tr.setLayoutParams(layout);


                TextView data = new TextView(getContext());
                TextView status = new TextView(getContext());
                TextView aceita = new TextView(getContext());
                TextView recusada = new TextView(getContext());
                ImageView detalhes = new ImageView(getContext());

                data.setText(getDate(questao.get(i).getCreated_at()));
                data.setTextColor(getResources().getColor(R.color.user_fragment_text_dados));

                status.setText(questao.get(i).getStatus());
                status.setTextColor(getResources().getColor(R.color.colorAccent));
                status.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                aceita.setText("" + questao.get(i).getAceita().size());
                aceita.setTextColor(getResources().getColor(R.color.colorPrimary));

                recusada.setText("" + questao.get(i).getRecusada().size());
                recusada.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                detalhes.setImageDrawable(getResources().getDrawable(R.drawable.ic_keyboard_arrow_down_black_24dp));




                tr.addView(data);
                tr.addView(aceita);
                tr.addView(recusada);
                tr.addView(status);
                tr.addView(detalhes);

                tl.addView(tr);
            }
        }
    }

    private String getDate(String dataEmUmFormato) throws ParseException {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        Date data = formato.parse(dataEmUmFormato);
        formato.applyPattern("dd/MM/yyyy");
        String dataFormatada = formato.format(data);

        return dataFormatada;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        Activity a;

        if (context instanceof Activity){
            a = (Activity) context;
            titleListener = (TitleListener) a;
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater){
        getActivity().getMenuInflater().inflate(R.menu.toolbar_home_sair,menu);
        super.onCreateOptionsMenu(menu, menuInflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.tooblar_sair:
                logout();
                return true;
            default:
                break;
        }

        return false;
    }

    private void getDados(){
        Gson gson  = new GsonBuilder().registerTypeAdapter( GetHistoricoRes.class, new GetHistoricoDes()).create();

        HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit(getContext());

        QuestaoInterface questaoInterface = retrofit.getRetrofit().create(QuestaoInterface.class);

        Call<GetHistoricoRes> call = questaoInterface.getHistorico();
        call.enqueue(new Callback<GetHistoricoRes>() {
            @Override
            public void onResponse(Call<GetHistoricoRes> call, retrofit2.Response<GetHistoricoRes> response) {
                if(response.code() == 200) {
                    setGraficos(response.body().getHistorico());
                    try {
                        setListQuestao(response.body().getQuestoes());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    progress.setVisibility(View.GONE);
                    form.setVisibility(View.VISIBLE);
                }else{
                    progress.setVisibility(View.GONE);
                    falha.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(Call<GetHistoricoRes> call, Throwable t) {
                falha.setVisibility(View.VISIBLE);
                progress.setVisibility(View.GONE);
            }
        });
    }

    private void logout(){
        Realm realm = Realm.getDefaultInstance();
        Token t = Token.getLoginAttemp();
        realm.beginTransaction();
        t.setAccess_token(null);
        t.setRefresh_token(null);
        t.setTime(0);
        realm.commitTransaction();
        realm.close();

        Intent intent = new Intent(getContext(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }
}
