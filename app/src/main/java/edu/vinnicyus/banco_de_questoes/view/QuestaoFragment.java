package edu.vinnicyus.banco_de_questoes.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import edu.vinnicyus.banco_de_questoes.ProgressViewStartEnd;
import edu.vinnicyus.banco_de_questoes.R;
import edu.vinnicyus.banco_de_questoes.request.HttpGlobalRetrofit;
import edu.vinnicyus.banco_de_questoes.request.api.AreaConhecimentoInterface;
import edu.vinnicyus.banco_de_questoes.request.api.QuestaoInterface;
import edu.vinnicyus.banco_de_questoes.request.deserializer.AreaConhecimentoDes;
import edu.vinnicyus.banco_de_questoes.request.deserializer.GetQntdQuestoesDes;
import edu.vinnicyus.banco_de_questoes.request.deserializer.QuestoesSelecionadasDes;
import edu.vinnicyus.banco_de_questoes.request.models.request.GetQntdQuestoes;
import edu.vinnicyus.banco_de_questoes.request.models.request.QuestoesSelecionadasModel;
import edu.vinnicyus.banco_de_questoes.request.models.response.AreaConhecimentoRes;
import edu.vinnicyus.banco_de_questoes.request.models.response.GetQntdQuestoesRes;
import edu.vinnicyus.banco_de_questoes.request.models.response.QuestoesSelecionadasRes;
import edu.vinnicyus.banco_de_questoes.validator.ValidatorCodesResponse;
import edu.vinnicyus.banco_de_questoes.view.Questao.SelecionarQuestoesActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class QuestaoFragment extends Fragment {

    private LinearLayout linearLayout;
    private ArrayList<AreaConhecimentoRes> areaConhecimentoEncadeadas;
    private TextView qntd_questao;
    private View total_progress;
    private ArrayList<Integer> ids_areas;
    private TitleListener titleListener;
    private ProgressViewStartEnd pv;
    private View falha;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_questao, container, false);
        setHasOptionsMenu(true);
        ids_areas = new ArrayList<>();
        linearLayout = view.findViewById(R.id.areas_conhecimento);
        qntd_questao = view.findViewById(R.id.qntdQuestao);
        total_progress = view.findViewById(R.id.total_progress);

        pv = new ProgressViewStartEnd(qntd_questao,total_progress,getResources());
        titleListener.setTitleActionBar("Elaborar Prova");

        falha = view.findViewById(R.id.falha);
        falha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(areaConhecimentoEncadeadas == null) {
                    getAreas();
                }
                falha.setVisibility(View.GONE);
            }
        });
        return view;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        Activity a;

        if (context instanceof Activity){
            a = (Activity) context;
            titleListener = (TitleListener) a;
        }

    }
    @Override
    public void onStart() {
        super.onStart();
        if(areaConhecimentoEncadeadas == null) {
            getAreas();
        }
    }

    public void getAreasConhecimento(){
        for(int i=0; i<areaConhecimentoEncadeadas.size();i++) {

            LinearLayout linear = new LinearLayout(getContext());
            LinearLayout.LayoutParams layout = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layout.setMargins(0,0,0, getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin));
            linear.setLayoutParams(layout);
            linear.setOrientation(LinearLayout.HORIZONTAL);

            final ImageButton image = new ImageButton(getContext());
            image.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            image.setBackground(getResources().getDrawable(R.drawable.image_button_areas));
            image.setImageResource(R.drawable.ic_add_black_24dp);
            image.setPadding(
                    5,5,5,5
            );

            CheckBox checkBox = new CheckBox(getContext());
            checkBox.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            checkBox.setText(areaConhecimentoEncadeadas.get(i).getArea_de_conhecimento());

            linear.addView(image);
            linear.addView(checkBox);
            linearLayout.addView(linear);

            final LinearLayout linear3 = new LinearLayout(getContext());
            LinearLayout.LayoutParams layout3 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layout3.setMargins(0,0,0, 0);
            linear3.setLayoutParams(layout3);
            linear3.setOrientation(LinearLayout.VERTICAL);
            linear3.setVisibility(View.GONE);

            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(linear3.getVisibility() == View.GONE) {
                        linear3.setVisibility(View.VISIBLE);
                        image.setImageResource(R.drawable.ic_remove_black_24dp);
                    }else{
                        linear3.setVisibility(View.GONE);
                        image.setImageResource(R.drawable.ic_add_black_24dp);
                    }
                }
            });
            final CheckBox ch[] = new CheckBox[areaConhecimentoEncadeadas.get(i).getSub_categoria_id().size()];
            final int pos = i;

            for (int y=0;y<areaConhecimentoEncadeadas.get(i).getSub_categoria_id().size();y++) {
                final int pos_s = y;
                ch[y] = new CheckBox(getContext());
                ch[y].setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                ch[y].setText(areaConhecimentoEncadeadas.get(i).getSub_categoria_id().get(y).getArea_de_conhecimento());
                ch[y].setId(areaConhecimentoEncadeadas.get(i).getSub_categoria_id().get(y).getId());

                ch[y].setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        selecionar(ch[pos_s].getId());
                    }
                });
                LinearLayout linear2 = new LinearLayout(getContext());
                LinearLayout.LayoutParams layout2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                layout2.setMargins(getResources().getDimensionPixelSize(R.dimen.questao_fragment_sub_categoria),0,0, getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin));
                linear2.setLayoutParams(layout2);
                linear2.setOrientation(LinearLayout.HORIZONTAL);

                linear2.addView(ch[y]);
                linear3.addView(linear2);
            }

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    for (int a=0;a<areaConhecimentoEncadeadas.get(pos).getSub_categoria_id().size();a++) {
                        ch[a].setChecked(b);
                    }
                }
            });
            linearLayout.addView(linear3);
        }
    }

    private void selecionar(int id){
        if(!ids_areas.contains(id)){
            ids_areas.add(id);
        }else{
            ids_areas.remove((Integer) id);
        }
        pv.show(true);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getQntdQuestoes();
            }
        }, 50);

    }

    private void getQntdQuestoes(){

        Gson gson  = new GsonBuilder().registerTypeAdapter( GetQntdQuestoesRes.class, new GetQntdQuestoesDes()).create();
        HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit(getContext());
        AreaConhecimentoInterface areaConhecimentoInterface = retrofit.getRetrofit().create(AreaConhecimentoInterface.class);
        GetQntdQuestoes getQntdQuestoes = new GetQntdQuestoes();
        getQntdQuestoes.setIds(ids_areas);
        Call<GetQntdQuestoesRes> call = areaConhecimentoInterface.getQntdQuestao(getQntdQuestoes);
        call.enqueue(new Callback<GetQntdQuestoesRes>() {
            @Override
            public void onResponse(Call<GetQntdQuestoesRes> call, retrofit2.Response<GetQntdQuestoesRes> response) {
                if(response.code() == 200) {
                    qntd_questao.setText("Total: " + response.body().getQntd());
                    pv.show(false);
                }
            }

            @Override
            public void onFailure(Call<GetQntdQuestoesRes> call, Throwable t) {
                //Toast.makeText(getContext(), "Error de Conexão!", Toast.LENGTH_SHORT).show();
                pv.show(false);
            }
        });

    }

    public void getAreas(){

        Gson gson  = new GsonBuilder().registerTypeAdapter( AreaConhecimentoRes.class, new AreaConhecimentoDes()).create();

        HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit(getContext());

        AreaConhecimentoInterface areaConhecimentoInterface = retrofit.getRetrofit().create(AreaConhecimentoInterface.class);
        Call<ArrayList<AreaConhecimentoRes>> call = areaConhecimentoInterface.getAreasEncadeada();
        call.enqueue(new Callback<ArrayList<AreaConhecimentoRes>>() {
            @Override
            public void onResponse(Call<ArrayList<AreaConhecimentoRes>> call, retrofit2.Response<ArrayList<AreaConhecimentoRes>> response) {
                if (response.code() == 200){
                    areaConhecimentoEncadeadas = response.body();
                    getAreasConhecimento();
                }else {
                    Toast.makeText(getContext(), "Error de Conexão!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<AreaConhecimentoRes>> call, Throwable t) {
                linearLayout.setVisibility(View.GONE);
                total_progress.setVisibility(View.GONE);
                falha.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater){
        getActivity().getMenuInflater().inflate(R.menu.toolbar_questao_fragment,menu);
        super.onCreateOptionsMenu(menu, menuInflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.tooblar_sair:
                next();
                return true;
            default:
                break;
        }

        return false;
    }

    private void next(){
        if(ids_areas.isEmpty()){
            Toast.makeText(getContext(), "Escolha algum tema!", Toast.LENGTH_SHORT).show();
        }else{
            getQuestoesSelecionadas();
        }
    }

    public void getQuestoesSelecionadas(){
        try {
            Gson gson  = new GsonBuilder().registerTypeAdapter( QuestoesSelecionadasRes.class, new QuestoesSelecionadasDes()).create();

            HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit(getContext());

            AreaConhecimentoInterface tokenInterface = retrofit.getRetrofit().create(AreaConhecimentoInterface.class);
            QuestoesSelecionadasModel questoesSelecionadasModel = new QuestoesSelecionadasModel();
            questoesSelecionadasModel.setIds(ids_areas);
            Call<ArrayList<QuestoesSelecionadasRes>> call = tokenInterface.getQuestoesProva(questoesSelecionadasModel);
            call.enqueue(new Callback<ArrayList<QuestoesSelecionadasRes>>() {
                @Override
                public void onResponse(Call<ArrayList<QuestoesSelecionadasRes>> call, retrofit2.Response<ArrayList<QuestoesSelecionadasRes>> response) {
                    if (response.code() == 200){
                        Intent intent = new Intent(getContext(),SelecionarQuestoesActivity.class);
                        intent.putExtra("questoes", response.body());
                        startActivity(intent);
                    }else{
                        Toast.makeText(getContext(), "Error de Conexão!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<QuestoesSelecionadasRes>> call, Throwable t) {
                    Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
            Thread.sleep(1000);
        } catch (InterruptedException e) {}
    }
}
