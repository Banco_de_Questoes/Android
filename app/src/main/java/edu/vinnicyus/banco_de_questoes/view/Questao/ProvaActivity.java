package edu.vinnicyus.banco_de_questoes.view.Questao;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import edu.vinnicyus.banco_de_questoes.R;
import edu.vinnicyus.banco_de_questoes.models.Usuario;
import edu.vinnicyus.banco_de_questoes.request.models.response.QuestoesSelecionadasRes;
import io.realm.Realm;

public class ProvaActivity extends AppCompatActivity {

    private LinearLayout lista_questoes;
    private EditText educador;
    private EditText titulo;
    private EditText data;
    private EditText valor;
    private TextView btn_edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prova);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Prova Concluida");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        lista_questoes = findViewById(R.id.lista_questao);




        educador = findViewById(R.id.educador);
        titulo = findViewById(R.id.titulo);
        data = findViewById(R.id.data);
        valor = findViewById(R.id.valor);

        SimpleDateFormat formataData = new SimpleDateFormat("dd-MM-yyyy");
        Date data_atual = new Date();
        String dataFormatada = formataData.format(data_atual);
        data.setText(dataFormatada);
        valor.setText("0.0");
        titulo.setText("A preencher!");

        Realm realm = Realm.getDefaultInstance();
        Usuario u = Usuario.getUser();
        educador.setText(u.getName());
        realm.close();


        educador.setEnabled(false);
        data.setEnabled(false);

        btn_edit = findViewById(R.id.edit);
        btn_edit.setText("Salvar");
        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(btn_edit.getText().equals("Salvar")) {
                    validation();
                }else {
                    btn_edit.setText("Salvar");

                    titulo.setEnabled(true);
                    valor.setEnabled(true);
                }
            }
        });
    }

    private void validation(){
        boolean status = false;

        if (TextUtils.isEmpty(titulo.getText().toString())) {
            titulo.setError(getString(R.string.error_field_required));
            status = true;
        }

        if (TextUtils.isEmpty(valor.getText().toString())) {
            valor.setError(getString(R.string.error_field_required));
            status = true;
        }

        if(!status){
            btn_edit.setText("Editar");
            titulo.setEnabled(false);
            valor.setEnabled(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        ArrayList<QuestoesSelecionadasRes> qs = getIntent().getParcelableArrayListExtra("questoes");
        listarProva(qs);
    }

    private void listarProva(ArrayList<QuestoesSelecionadasRes> qs){

        for (int i=0;i<qs.size();i++) {
            TextView enunciado = new TextView(this);
            LinearLayout alternativas = new LinearLayout(this);
            LinearLayout questao = new LinearLayout(this);


            LinearLayout.LayoutParams layoutParams_alternativa = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams_alternativa.setMargins(getResources().getDimensionPixelSize(R.dimen.activity_alternativas), getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin), 0, 0);

            LinearLayout.LayoutParams layoutParams_enuciado = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams_enuciado.setMargins(getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin), getResources().getDimensionPixelSize(R.dimen.activity_alternativas), getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin),getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin));

            LinearLayout.LayoutParams layoutParams_lista = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            alternativas.setOrientation(LinearLayout.VERTICAL);
            alternativas.setLayoutParams(layoutParams_alternativa);

            questao.setLayoutParams(layoutParams_lista);

            enunciado.setLayoutParams(layoutParams_enuciado);
            enunciado.setText((i+1)+" - " + qs.get(i).getEnunciado());
            questao.setOrientation(LinearLayout.VERTICAL);
            questao.addView(enunciado);

            for (int j = 0; j < qs.get(i).getAlternativas().length; j++) {
                TextView textView1 = new TextView(this);

                textView1.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                textView1.setText((j + 1) + " - " + qs.get(i).getAlternativas()[j].toString());
                if (j == qs.get(i).getAlternativa_correta()) {
                    textView1.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
                alternativas.addView(textView1);
            }
            questao.addView(alternativas);
            View view = new View(this);
            LinearLayout.LayoutParams layoutParams_view = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 1);
            layoutParams_view.setMargins(2, getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin), 2, 0);

            view.setLayoutParams(layoutParams_view);
            view.setBackgroundColor(getResources().getColor(R.color.user_fragment_text_info));

            questao.addView(view);
            lista_questoes.addView(questao);
        }
    }

}
