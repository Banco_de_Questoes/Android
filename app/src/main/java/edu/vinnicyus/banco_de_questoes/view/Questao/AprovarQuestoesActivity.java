package edu.vinnicyus.banco_de_questoes.view.Questao;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import edu.vinnicyus.banco_de_questoes.ProgressViewStartEnd;
import edu.vinnicyus.banco_de_questoes.R;
import edu.vinnicyus.banco_de_questoes.request.HttpGlobalRetrofit;
import edu.vinnicyus.banco_de_questoes.request.api.QuestaoInterface;
import edu.vinnicyus.banco_de_questoes.request.deserializer.AceitaRecusaDes;
import edu.vinnicyus.banco_de_questoes.request.deserializer.QuestoesSelecionadasDes;
import edu.vinnicyus.banco_de_questoes.request.models.response.AceitaRecusaRes;
import edu.vinnicyus.banco_de_questoes.request.models.response.QuestoesSelecionadasRes;
import retrofit2.Call;
import retrofit2.Callback;

public class AprovarQuestoesActivity extends AppCompatActivity{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    //private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private RelativeLayout mViewPager;
    private ArrayList<QuestoesSelecionadasRes> all_qs;
    private QuestoesSelecionadasRes atual_qs;

    private ProgressViewStartEnd mPV;

    private Button btn_aceita;
    private Button btn_recusa;
    private int currentItem = 0;
    private TextView textView;
    private TextView enunciado;
    private TextView area_conhecimento_info;
    private LinearLayout alternativas;
    private LinearLayout questao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aprovar_questoes);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Avaliar");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.

        mViewPager = findViewById(R.id.corpo);
        View mProgressBar = findViewById(R.id.progress_bar);
        mPV = new ProgressViewStartEnd(mViewPager, mProgressBar, getResources());
        textView = (TextView) findViewById(R.id.section_label);
        enunciado = findViewById(R.id.enunciado);
        alternativas = findViewById(R.id.alternativas);
        questao = findViewById(R.id.questao);
        area_conhecimento_info = findViewById(R.id.area_conheicmento_info);
        btn_aceita = findViewById(R.id.aceitar);
        btn_recusa = findViewById(R.id.recusar);
    }

    private void alterLayoutFragment(){

        textView.setText(getString(R.string.section_format_selecionar, all_qs.size(), 10));
        area_conhecimento_info.setText(getString(R.string.aprovar_questao_are_conhecimento, atual_qs.getSub_categoria().getArea_de_conhecimento()));
        enunciado.setText("*   " +atual_qs.getEnunciado());

        for (int i = 0; i< atual_qs.getAlternativas().length; i++){
            TextView textView1 = new TextView(getApplicationContext());

            textView1.setLayoutParams( new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            textView1.setText((i+1)+" - "+atual_qs.getAlternativas()[i].toString());
            if(i == atual_qs.getAlternativa_correta()){
                textView1.setTextColor(getResources().getColor(R.color.colorPrimary));
            }
            alternativas.addView(textView1);
        }
        btn_aceita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAceita(atual_qs);
            }
        });

        btn_recusa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRecusa(atual_qs);
            }
        });
        mPV.show(false);
    }


    @Override
    protected void onStart() {
        super.onStart();
        mPV.show(true);
        getQuestoesSelecionadas();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
    public void getQuestoesSelecionadas(){
            Gson gson  = new GsonBuilder().registerTypeAdapter( QuestoesSelecionadasRes.class, new QuestoesSelecionadasDes()).create();

            HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit(getApplicationContext());

            QuestaoInterface questaoInterface = retrofit.getRetrofit().create(QuestaoInterface.class);

            Call<ArrayList<QuestoesSelecionadasRes>> call = questaoInterface.getQuestoesPendentes();
            call.enqueue(new Callback<ArrayList<QuestoesSelecionadasRes>>() {
                @Override
                public void onResponse(Call<ArrayList<QuestoesSelecionadasRes>> call, retrofit2.Response<ArrayList<QuestoesSelecionadasRes>> response) {
                    if (response.code() == 200){
                        all_qs = response.body();
                        //mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
                        //mViewPager.setAdapter(mSectionsPagerAdapter);
                        currentItem = response.body().size() -1;
                        atual_qs = response.body().get(currentItem);
                        alterLayoutFragment();
                    }else{
                        Toast.makeText(getApplicationContext(), "422 '"+response.code(), Toast.LENGTH_SHORT).show();
                    }
                    mPV.show(false);
                }

                @Override
                public void onFailure(Call<ArrayList<QuestoesSelecionadasRes>> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    mPV.show(false);
                }
            });

    }

    public void setFragmentAtual(){
        if(currentItem == 0){
            finish();
        }else {
            currentItem--;
            atual_qs = all_qs.get(currentItem);
            alternativas.removeAllViews();
            alterLayoutFragment();
        }
    }

    private void setAceita(final QuestoesSelecionadasRes qs2){
        mPV.show(true);
        Gson gson  = new GsonBuilder().registerTypeAdapter( AceitaRecusaRes.class, new AceitaRecusaDes()).create();

        HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit(this);

        QuestaoInterface questaoInterface = retrofit.getRetrofit().create(QuestaoInterface.class);

        Call<AceitaRecusaRes> call = questaoInterface.setAceita(qs2.getId());
        call.enqueue(new Callback<AceitaRecusaRes>() {
            @Override
            public void onResponse(Call<AceitaRecusaRes> call, retrofit2.Response<AceitaRecusaRes> response) {
                if (response.code() == 200){
                    all_qs.remove(qs2);
                   setFragmentAtual();
                }else{
                    finish();
                }
            }

            @Override
            public void onFailure(Call<AceitaRecusaRes> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Tente Novamente!", Toast.LENGTH_SHORT).show();
                mPV.show(false);
            }
        });
    }
    private void setRecusa(final QuestoesSelecionadasRes qs2){
        mPV.show(true);
        Gson gson  = new GsonBuilder().registerTypeAdapter( AceitaRecusaRes.class, new AceitaRecusaDes()).create();

        HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit(this);

        QuestaoInterface questaoInterface = retrofit.getRetrofit().create(QuestaoInterface.class);

        Call<AceitaRecusaRes> call = questaoInterface.setRecusa(qs2.getId());
        call.enqueue(new Callback<AceitaRecusaRes>() {
            @Override
            public void onResponse(Call<AceitaRecusaRes> call, retrofit2.Response<AceitaRecusaRes> response) {
                if (response.code() == 200){
                    all_qs.remove(qs2);
                    setFragmentAtual();
                }else{
                    finish();
                }
            }

            @Override
            public void onFailure(Call<AceitaRecusaRes> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Tente Novamente!", Toast.LENGTH_SHORT).show();
                mPV.show(false);
            }
        });
    }
}
