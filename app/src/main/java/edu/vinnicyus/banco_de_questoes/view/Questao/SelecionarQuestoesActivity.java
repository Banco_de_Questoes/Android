package edu.vinnicyus.banco_de_questoes.view.Questao;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import edu.vinnicyus.banco_de_questoes.R;
import edu.vinnicyus.banco_de_questoes.request.models.response.QuestoesSelecionadasRes;

public class SelecionarQuestoesActivity extends AppCompatActivity implements FragmentListener{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    public ViewPager mViewPager;

    private ArrayList<QuestoesSelecionadasRes> qs;

    private ArrayList<QuestoesSelecionadasRes> ids_questoes_selecionadas;
    private TextView selecionadas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ids_questoes_selecionadas = new ArrayList<>();
        Intent intent = getIntent();
        qs = intent.getExtras().getParcelableArrayList("questoes");

        setContentView(R.layout.activity_selecionar_questoes);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Escolhas as Questões:");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        selecionadas = findViewById(R.id.questoes_selecionadas);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_questao_fragment, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                return true;
            case R.id.tooblar_sair:
                nextProva();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    public boolean isChecked(QuestoesSelecionadasRes id) {
        return ids_questoes_selecionadas.contains((QuestoesSelecionadasRes) id);
    }

    public void checkboxChange(QuestoesSelecionadasRes id) {
        if(!ids_questoes_selecionadas.contains((QuestoesSelecionadasRes) id)){
            ids_questoes_selecionadas.add(id);
        }else{
            ids_questoes_selecionadas.remove((QuestoesSelecionadasRes) id);
        }
        selecionadas.setText(ids_questoes_selecionadas.size()+" - Questões");
    }

    private void nextProva(){
        if(ids_questoes_selecionadas.size() > 0) {
            Intent intent = new Intent(this, ProvaActivity.class);
            intent.putExtra("questoes", ids_questoes_selecionadas);
            startActivity(intent);
            finish();
        }else{
            Toast.makeText(this,"Nenhuma questão selecionada",Toast.LENGTH_SHORT).show();
        }
    }
    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment{
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_format_selecionar";
        private static final String TOTAL_NUMBER = "total_number";

        private FragmentListener mListener;

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber, QuestoesSelecionadasRes questoesSelecionadasRes, int size) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            args.putInt(TOTAL_NUMBER, size);
            args.putParcelable("questao",questoesSelecionadasRes);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_selecionar_questoes, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format_selecionar, getArguments().getInt(ARG_SECTION_NUMBER), getArguments().getInt(TOTAL_NUMBER)));

            QuestoesSelecionadasRes qs = getArguments().getParcelable("questao");

            TextView enunciado = rootView.findViewById(R.id.enunciado);
            LinearLayout alternativas = rootView.findViewById(R.id.alternativas);
            LinearLayout questao = rootView.findViewById(R.id.questao);

            enunciado.setText("*   " +qs.getEnunciado());

            for (int i = 0; i< qs.getAlternativas().length; i++){
                TextView textView1 = new TextView(getContext());

                textView1.setLayoutParams( new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                textView1.setText((i+1)+" - "+qs.getAlternativas()[i].toString());
                if(i == qs.getAlternativa_correta()){
                    textView1.setTextColor(getResources().getColor(R.color.colorPrimary));
                }

                alternativas.addView(textView1);


            }

            CheckBox checkBox = new CheckBox(getContext());

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(getResources().getDimensionPixelSize(R.dimen.activity_checkbox), getResources().getDimensionPixelSize(R.dimen.activity_checkbox),0,0);
            checkBox.setLayoutParams(layoutParams);
            checkBox.setText("Adicionar");
            final QuestoesSelecionadasRes qs2 = qs;
            checkBox.setChecked(mListener.isChecked(qs2));
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                   if(mListener != null) mListener.checkboxChange(qs2);
                }
            });

            questao.addView(checkBox);

            return rootView;
        }

        public void onAttach(Context context) {
            super.onAttach(context);
            Activity a;

            if (context instanceof Activity){
                a = (Activity) context;
                mListener = (FragmentListener) a;
            }

        }

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {

            return PlaceholderFragment.newInstance(position + 1, qs.get(position), qs.size());
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return qs.size();
        }
    }



}
