package edu.vinnicyus.banco_de_questoes.view.Questao;

import edu.vinnicyus.banco_de_questoes.request.models.response.QuestoesSelecionadasRes;

public interface FragmentListener {

    public void checkboxChange(QuestoesSelecionadasRes id);
    public boolean isChecked(QuestoesSelecionadasRes id);
}
