package edu.vinnicyus.banco_de_questoes;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import edu.vinnicyus.banco_de_questoes.models.Token;
import edu.vinnicyus.banco_de_questoes.models.Usuario;
import edu.vinnicyus.banco_de_questoes.request.HttpGlobalRetrofit;
import edu.vinnicyus.banco_de_questoes.request.api.TokenInterface;
import edu.vinnicyus.banco_de_questoes.request.api.UsuarioInterface;
import edu.vinnicyus.banco_de_questoes.request.deserializer.ClientCredentialsDes;
import edu.vinnicyus.banco_de_questoes.request.deserializer.TokenDes;
import edu.vinnicyus.banco_de_questoes.request.deserializer.UsuarioDes;
import edu.vinnicyus.banco_de_questoes.request.models.request.GetTokenModel;
import edu.vinnicyus.banco_de_questoes.request.models.response.ClientCredentialsModel;
import edu.vinnicyus.banco_de_questoes.request.models.response.GetTokenResModel;
import edu.vinnicyus.banco_de_questoes.view.HomeActivity;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    private Realm realm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.email_login_form);
        mProgressView = findViewById(R.id.login_progress);

        Token teste = Token.getLoginAttemp();
        realm = Realm.getDefaultInstance();
        if(teste == null) {

            realm.beginTransaction();
            Token token = realm.createObject(Token.class);
            token.setIdentificar(2);
            realm.commitTransaction();
        }else{

            Token t = Token.getLoginAttemp();
            if(t.getAccess_token() != null){
                Intent intent = new Intent(this,
                        HomeActivity.class);
                realm.close();
                startActivity(intent);
                finish();
            }
        }
    }

    private void attemptLogin() {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            credentialsTask(email, password);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


   private void mAuthTask(final String email,final String password,final ClientCredentialsModel ccm) {
       Handler handler = new Handler();
       handler.postDelayed(new Runnable() {
           @Override
           public void run() {
                Gson gson  = new GsonBuilder().registerTypeAdapter( GetTokenResModel.class, new TokenDes()).create();

                HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit(getApplicationContext());

                TokenInterface tokenInterface = retrofit.getRetrofit().create(TokenInterface.class);

                GetTokenModel getT = new GetTokenModel();
                getT.setGrant_type("password");
                getT.setClient_id(""+ccm.getClient_id());
                getT.setClient_secret(ccm.getClient_secret());
                getT.setUsername(email);
                getT.setPassword(password);
                getT.setApi("professor");

                Call<GetTokenResModel> call = tokenInterface.createToken(getT);
                call.enqueue(new Callback<GetTokenResModel>() {
                    @Override
                    public void onResponse(Call<GetTokenResModel> call, retrofit2.Response<GetTokenResModel> response) {
                        if(response.code() == 200) {
                            Realm re = Realm.getDefaultInstance();
                            re.beginTransaction();
                            Token token = Token.getLoginAttemp();
                            token.setAccess_token(response.body().getAcess_token());
                            token.setRefresh_token(response.body().getRefresh_token());
                            token.setTime(response.body().getExpiracao());
                            re.commitTransaction();

                            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();
                        }else if(response.code() == 422){
                            //mensagem(response.errorBody().toString());
                        }else{
                            mensagem("Login/Senha Invalidos!");
                        }
                        showProgress(false);
                    }
                    @Override
                    public void onFailure(Call<GetTokenResModel> call, Throwable t) {
                       mensagem("Falha de conexão, tente novamente!");
                       showProgress(false);
                    }
                });
           }
       }, 1500);
    }


    public void credentialsTask(final String ctLogin, final String ctPassword){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                Gson gson  = new GsonBuilder().registerTypeAdapter( ClientCredentialsModel.class, new ClientCredentialsDes()).create();

                HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit(getApplicationContext());

                TokenInterface tokenInterface = retrofit.getRetrofit().create(TokenInterface.class);

                Call<ClientCredentialsModel> call = tokenInterface.getCredentials();
                call.enqueue(new Callback<ClientCredentialsModel>() {
                    @Override
                    public void onResponse(Call<ClientCredentialsModel> call, retrofit2.Response<ClientCredentialsModel> response) {
                        if(response.code() == 200) {
                                mAuthTask(ctLogin,ctPassword,response.body());
                        }else{
                            mensagem(response.errorBody().toString());
                            showProgress(false);
                        }

                    }

                    @Override
                    public void onFailure(Call<ClientCredentialsModel> call, Throwable t) {
                        mensagem("Falha de conexão S1");
                        showProgress(false);
                    }
                });
            }
        }, 1500);
    }



    public void mensagem(String mensagem){
        Toast.makeText(this,mensagem,Toast.LENGTH_LONG).show();
    }
}

