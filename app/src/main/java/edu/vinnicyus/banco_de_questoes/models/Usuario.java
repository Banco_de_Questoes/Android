package edu.vinnicyus.banco_de_questoes.models;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class Usuario extends RealmObject {

    private int identi;
    private int id;
    private String name;
    private String email;
    private String telefone;
    private String sexo;
    private String matricula_prof;
    private String cpf;
    private String role;

    public Usuario(){}

    public void setValues(Usuario u){
        this.name = u.getName();
        this.cpf = u.getCpf();
        this.id = u.getId();
        this.email = u.getEmail();
        this.sexo = u.getSexo();
        this.telefone = u.getTelefone();
        this.matricula_prof = u.getMatricula_prof();
        this.role = u.getRole();
    }

    public int getIdenti() {
        return identi;
    }

    public void setIdenti(int identi) {
        this.identi = identi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getMatricula_prof() {
        return matricula_prof;
    }

    public void setMatricula_prof(String matricula_prof) {
        this.matricula_prof = matricula_prof;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public static Usuario getUser(){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Usuario> u;
        u = realm.where(Usuario.class).equalTo("identi", 1).findAll();
        return u.size() == 0 ? null : u.first();
    }

    public class ErrorsUser{
        private ArrayList name;
        private ArrayList email;
        private ArrayList matricula_prof;
        private ArrayList telefone;
        private ArrayList sexo;
        private ArrayList cpf;

        public ArrayList getName() {
            return name;
        }

        public void setName(ArrayList name) {
            this.name = name;
        }

        public ArrayList getEmail() {
            return email;
        }

        public void setEmail(ArrayList email) {
            this.email = email;
        }

        public ArrayList getMatricula_prof() {
            return matricula_prof;
        }

        public void setMatricula_prof(ArrayList matricula_prof) {
            this.matricula_prof = matricula_prof;
        }

        public ArrayList getTelefone() {
            return telefone;
        }

        public void setTelefone(ArrayList telefone) {
            this.telefone = telefone;
        }

        public ArrayList getSexo() {
            return sexo;
        }

        public void setSexo(ArrayList sexo) {
            this.sexo = sexo;
        }

        public ArrayList getCpf() {
            return cpf;
        }

        public void setCpf(ArrayList cpf) {
            this.cpf = cpf;
        }
    }
}
