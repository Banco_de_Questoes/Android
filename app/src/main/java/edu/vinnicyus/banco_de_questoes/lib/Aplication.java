package edu.vinnicyus.banco_de_questoes.lib;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class Aplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("bq.realm")
                .schemaVersion(1)
                .build();
        Realm.setDefaultConfiguration(config);
    }
}
