package edu.vinnicyus.banco_de_questoes.request;

import android.content.Context;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HttpGlobalRetrofit {

    private Retrofit retrofit;
    private static final String ENDERECO_SERVIDOR="http://10.1.1.8:8000/";
    private Context context;

    public HttpGlobalRetrofit(Context c) {
        this.context = c;
        retrofit = new Retrofit.Builder().baseUrl(ENDERECO_SERVIDOR).addConverterFactory(GsonConverterFactory.create())
                .client(aplicarInterceptor())
                .build();
    }

    private OkHttpClient aplicarInterceptor(){
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new HttpInterceptor()).addInterceptor(new HttpInterceptorResponse(context))
                .build();
        return client;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}
