package edu.vinnicyus.banco_de_questoes.request.models.response;

import java.util.ArrayList;

public class AreaConhecimentoRes {

    private int id;
    private String area_de_conhecimento;
    private ArrayList<SubCategoria> sub_categoria_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getArea_de_conhecimento() {
        return area_de_conhecimento;
    }

    public void setArea_de_conhecimento(String area_de_conhecimento) {
        this.area_de_conhecimento = area_de_conhecimento;
    }

    public ArrayList<SubCategoria> getSub_categoria_id() {
        return sub_categoria_id;
    }

    public void setSub_categoria_id(ArrayList<SubCategoria> sub_categoria_id) {
        this.sub_categoria_id = sub_categoria_id;
    }

    public class SubCategoria{
        private int id;
        private String area_de_conhecimento;
        private int sub_categoria_id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getArea_de_conhecimento() {
            return area_de_conhecimento;
        }

        public void setArea_de_conhecimento(String area_de_conhecimento) {
            this.area_de_conhecimento = area_de_conhecimento;
        }

        public int getSub_categoria_id() {
            return sub_categoria_id;
        }

        public void setSub_categoria_id(int sub_categoria_id) {
            this.sub_categoria_id = sub_categoria_id;
        }
    }

    public class Errors{

    }
}
