package edu.vinnicyus.banco_de_questoes.request.models.response;

import android.os.Parcel;
import android.os.Parcelable;

public class SubCategoriaRes implements Parcelable {
        private int id;
        private String area_de_conhecimento;
        private int sub_categoria_id;
        private String created_at;

    public SubCategoriaRes(int id, String area_de_conhecimento, int sub_categoria_id, String created_at) {
        this.id = id;
        this.area_de_conhecimento = area_de_conhecimento;
        this.sub_categoria_id = sub_categoria_id;
        this.created_at = created_at;
    }

    public int getId() {
        return id;
    }

    public String getArea_de_conhecimento() {
        return area_de_conhecimento;
    }

    public int getSub_categoria_id() {
        return sub_categoria_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.area_de_conhecimento);
        dest.writeInt(this.sub_categoria_id);
        dest.writeString(this.created_at);
    }

    protected SubCategoriaRes(Parcel in) {
        this.id = in.readInt();
        this.area_de_conhecimento = in.readString();
        this.sub_categoria_id = in.readInt();
        this.created_at = in.readString();
    }

    public static final Creator<SubCategoriaRes> CREATOR = new Creator<SubCategoriaRes>() {
        @Override
        public SubCategoriaRes createFromParcel(Parcel source) {
            return new SubCategoriaRes(source);
        }

        @Override
        public SubCategoriaRes[] newArray(int size) {
            return new SubCategoriaRes[size];
        }
    };
}
