package edu.vinnicyus.banco_de_questoes.request.models.response;


import java.util.ArrayList;

public class QuestaoRes {

    private int id;
    private String enunciado;
    private String[] alternativas;
    private int alternativa_correta;
    private String status;
    private ArrayList<Integer> aceita;
    private ArrayList<Integer> recusada;
    private int nivel;
    private int sub_categoria;
    private int professor_id;
    private String created_at;
    private String updated_at;

    public int getId() {
        return id;
    }

    public String getEnunciado() {
        return enunciado;
    }

    public String[] getAlternativas() {
        return alternativas;
    }

    public int getAlternativa_correta() {
        return alternativa_correta;
    }

    public int getNivel() {
        return nivel;
    }

    public int getSub_categoria() {
        return sub_categoria;
    }

    public int getProfessor_id() {
        return professor_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getStatus() {
        return status;
    }

    public ArrayList<Integer> getAceita() {
        return aceita;
    }

    public ArrayList<Integer> getRecusada() {
        return recusada;
    }
}
