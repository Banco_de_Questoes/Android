package edu.vinnicyus.banco_de_questoes.request.deserializer;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import edu.vinnicyus.banco_de_questoes.request.models.response.GetHistoricoRes;

public class GetHistoricoDes implements JsonDeserializer<Object> {
    @Override
    public Object deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonElement token = json.getAsJsonObject();

        return (new Gson().fromJson( token, GetHistoricoRes.class));
    }
}
