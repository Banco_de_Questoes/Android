package edu.vinnicyus.banco_de_questoes.request;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import edu.vinnicyus.banco_de_questoes.models.Token;
import edu.vinnicyus.banco_de_questoes.request.api.TokenInterface;
import edu.vinnicyus.banco_de_questoes.request.deserializer.ClientCredentialsDes;
import edu.vinnicyus.banco_de_questoes.request.deserializer.TokenDes;
import edu.vinnicyus.banco_de_questoes.request.models.request.GetTokenModel;
import edu.vinnicyus.banco_de_questoes.request.models.response.ClientCredentialsModel;
import edu.vinnicyus.banco_de_questoes.request.models.response.GetTokenResModel;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;

public class HttpRefreshToken {

    private Context context;

    public HttpRefreshToken(Context c){
        this.context = c;
    }

    private void getRefreshToken(final ClientCredentialsModel ccm){
        try {
            Gson gson = new GsonBuilder().registerTypeAdapter(GetTokenResModel.class, new TokenDes()).create();

            HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit(context);

            TokenInterface tokenInterface = retrofit.getRetrofit().create(TokenInterface.class);

            Realm r = Realm.getDefaultInstance();
            Token token = Token.getLoginAttemp();

            GetTokenModel getT = new GetTokenModel();
            getT.setGrant_type("refresh_token");
            getT.setRefresh_token(token.getRefresh_token());
            getT.setClient_id("" + ccm.getClient_id());
            getT.setClient_secret(ccm.getClient_secret());
            getT.setApi("professor");

            r.close();

            Call<GetTokenResModel> call = tokenInterface.createToken(getT);
            call.enqueue(new Callback<GetTokenResModel>() {
                @Override
                public void onResponse(Call<GetTokenResModel> call, retrofit2.Response<GetTokenResModel> response) {
                    if (response.code() == 200) {
                        Toast.makeText(context, "Conexão reestabelecida!", Toast.LENGTH_SHORT).show();
                        Realm re = Realm.getDefaultInstance();
                        re.beginTransaction();
                        Token token = Token.getLoginAttemp();
                        token.setAccess_token(response.body().getAcess_token());
                        token.setRefresh_token(response.body().getRefresh_token());
                        token.setTime(response.body().getExpiracao());
                        re.commitTransaction();
                        re.close();
                    }
                }

                @Override
                public void onFailure(Call<GetTokenResModel> call, Throwable t) {
                    Toast.makeText(context, "Internet com problemas, tente novamente! (S2)", Toast.LENGTH_SHORT).show();
                }
            });
            Thread.sleep(1000);
        } catch (InterruptedException e){

        }
    }

    public void credentialsTask(){
        try {
            Gson gson = new GsonBuilder().registerTypeAdapter(ClientCredentialsModel.class, new ClientCredentialsDes()).create();

            HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit(context);

            TokenInterface tokenInterface = retrofit.getRetrofit().create(TokenInterface.class);

            Call<ClientCredentialsModel> call = tokenInterface.getCredentials();
            call.enqueue(new Callback<ClientCredentialsModel>() {
                @Override
                public void onResponse(Call<ClientCredentialsModel> call, retrofit2.Response<ClientCredentialsModel> response) {
                    if (response.code() == 200) {
                        if (!response.body().getClient_secret().isEmpty()) {
                            getRefreshToken(response.body());
                        }
                    }
                }

                @Override
                public void onFailure(Call<ClientCredentialsModel> call, Throwable t) {
                    Toast.makeText(context, "Internet com problemas, tente novamente! (S1)", Toast.LENGTH_SHORT).show();
                }
            });
            Thread.sleep(1000);
        } catch (InterruptedException e){

        }
    }
}
