package edu.vinnicyus.banco_de_questoes.request.api;

import java.util.ArrayList;

import edu.vinnicyus.banco_de_questoes.request.models.request.GetQntdQuestoes;
import edu.vinnicyus.banco_de_questoes.request.models.request.QuestoesSelecionadasModel;
import edu.vinnicyus.banco_de_questoes.request.models.response.AreaConhecimentoRes;
import edu.vinnicyus.banco_de_questoes.request.models.response.GetQntdQuestoesRes;
import edu.vinnicyus.banco_de_questoes.request.models.response.QuestaoRes;
import edu.vinnicyus.banco_de_questoes.request.models.response.QuestoesSelecionadasRes;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AreaConhecimentoInterface {

    String rota = "api/area-conhecimento/";

    @GET(rota+"areas-encadeadas")
    Call<ArrayList<AreaConhecimentoRes>> getAreasEncadeada();

    @POST(rota+"getQntdQuestao")
    Call<GetQntdQuestoesRes> getQntdQuestao(@Body GetQntdQuestoes ids);

    @POST(rota+"getQuestoesProva")
    Call<QuestaoRes> getQuestoesProva(@Body ArrayList<Integer> ids);

    @POST(rota+"getQuestoesProva")
    Call<ArrayList<QuestoesSelecionadasRes>> getQuestoesProva(@Body QuestoesSelecionadasModel ids);
}
