package edu.vinnicyus.banco_de_questoes.request.models.request;

public class UsuarioModel {

    private String name;
    private String email;
    private String telefone;
    private String matricula_prof;
    private String cpf;

    public UsuarioModel(){}


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getMatricula_prof() {
        return matricula_prof;
    }

    public void setMatricula_prof(String matricula_prof) {
        this.matricula_prof = matricula_prof;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

}
