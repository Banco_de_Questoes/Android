package edu.vinnicyus.banco_de_questoes.request.models.response;

import java.util.ArrayList;

public class GetHistoricoRes {

    private ArrayList<Historico> historico;
    private ArrayList<QuestaoRes> questoes;

    public ArrayList<Historico> getHistorico() {
        return historico;
    }

    public ArrayList<QuestaoRes> getQuestoes() {
        return questoes;
    }

    public class Historico{
        private String mes;
        private int questoes_elaboradas;
        private int questoes_aprovadas;

        public String getMes() {
            return mes;
        }

        public int getQuestoes_elaboradas() {
            return questoes_elaboradas;
        }

        public int getQuestoes_aprovadas() {
            return questoes_aprovadas;
        }
    }

}
