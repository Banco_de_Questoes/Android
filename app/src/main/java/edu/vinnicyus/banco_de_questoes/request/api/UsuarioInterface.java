package edu.vinnicyus.banco_de_questoes.request.api;

import edu.vinnicyus.banco_de_questoes.models.Usuario;
import edu.vinnicyus.banco_de_questoes.request.models.request.UsuarioModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UsuarioInterface {

    String rota = "api/professor/";

    @GET(rota+"0")
    Call<Usuario> getUser();

    @PUT(rota+"{id}")
    Call<Usuario> updateUser(@Path("id") String id,@Body UsuarioModel usuario);
}
