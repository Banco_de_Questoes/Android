package edu.vinnicyus.banco_de_questoes.request;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.logging.Handler;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class HttpInterceptorResponse implements Interceptor {

    private Context context;

    public HttpInterceptorResponse(Context c) {
        this.context = c;
    }
    @Override
    public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            okhttp3.Response response = chain.proceed(request);
            // todo deal with the issues the way you need to
        switch ((int) response.code()) {
            case 500:
                android.os.Handler handler = new android.os.Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context,"Problemas com o Servidor, tente novamente!",Toast.LENGTH_SHORT).show();
                    }
                }, 2000);

                break;
            case 401:
                //Toast.makeText(context,"Usuario não autenticado, reestabelecendo conexão!",Toast.LENGTH_SHORT).show();
                HttpRefreshToken httpRefreshToken = new HttpRefreshToken(context);
                httpRefreshToken.credentialsTask();
                break;
            default:
                //Toast.makeText(context,"Usuario não autenticado, reestabelecendo conexão!"+ response.code(),Toast.LENGTH_SHORT).show();
                Log.d("Code Response", "onError: Hide spinner "+response.code()+ "  "+response.message());
                break;
        }
        return response;
    }
}

