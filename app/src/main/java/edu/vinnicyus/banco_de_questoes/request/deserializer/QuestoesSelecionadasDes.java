package edu.vinnicyus.banco_de_questoes.request.deserializer;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import edu.vinnicyus.banco_de_questoes.request.models.response.QuestoesSelecionadasRes;
import edu.vinnicyus.banco_de_questoes.request.models.response.SubCategoriaRes;

public class QuestoesSelecionadasDes implements JsonDeserializer<Object> {
    @Override
    public Object deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonElement j = json.getAsJsonObject();

        return (new Gson().fromJson( j, getType(QuestoesSelecionadasRes.class, SubCategoriaRes.class)));
    }

    public Type getType(final Class<?> rawClass,final Class<?> parameter) {
        return new ParameterizedType() {
            @Override
            public Type[] getActualTypeArguments() {
                return new Type[] {parameter};
            }
            @Override
            public Type getRawType() {
                return rawClass;
            }
            @Override
            public Type getOwnerType() {
                return null;
            }
        };
    }
}
