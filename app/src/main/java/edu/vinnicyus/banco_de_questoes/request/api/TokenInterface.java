package edu.vinnicyus.banco_de_questoes.request.api;

import edu.vinnicyus.banco_de_questoes.request.models.request.GetTokenModel;
import edu.vinnicyus.banco_de_questoes.request.models.response.ClientCredentialsModel;
import edu.vinnicyus.banco_de_questoes.request.models.response.GetTokenResModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface TokenInterface {

    @GET("credentials_client")
    Call<ClientCredentialsModel> getCredentials();

    @POST("oauth/token")
    Call<GetTokenResModel> createToken(@Body GetTokenModel getToken);
}
