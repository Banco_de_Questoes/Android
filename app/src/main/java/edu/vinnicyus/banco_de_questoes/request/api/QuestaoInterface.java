package edu.vinnicyus.banco_de_questoes.request.api;

import java.util.ArrayList;
import edu.vinnicyus.banco_de_questoes.request.models.response.AceitaRecusaRes;
import edu.vinnicyus.banco_de_questoes.request.models.response.GetHistoricoRes;
import edu.vinnicyus.banco_de_questoes.request.models.response.QuestoesSelecionadasRes;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface QuestaoInterface {

    String rota = "api/questao/";

    @GET(rota+"pendentes")
    Call<ArrayList<QuestoesSelecionadasRes>> getQuestoesPendentes();

    @GET(rota+"aceita/{id}")
    Call<AceitaRecusaRes> setAceita(@Path("id") int id);

    @GET(rota+"recusa/{id}")
    Call<AceitaRecusaRes> setRecusa(@Path("id") int id);

    @GET("api/professor/historico")
    Call<GetHistoricoRes> getHistorico();
}
