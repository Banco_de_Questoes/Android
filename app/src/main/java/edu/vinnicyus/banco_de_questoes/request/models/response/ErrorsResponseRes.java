package edu.vinnicyus.banco_de_questoes.request.models.response;

public class ErrorsResponseRes<T> {

    private String message;
    private T errors;

    public ErrorsResponseRes(){

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getErrors() {
        return errors;
    }

    public void setErrors(T errors) {
        this.errors = errors;
    }
}
