package edu.vinnicyus.banco_de_questoes.request;

import java.io.IOException;

import edu.vinnicyus.banco_de_questoes.models.Token;
import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class HttpInterceptor implements Interceptor{

    private static final String HEADER_AUTORIZACAO="Authorization";
    private static final String TIPO_TOKEN="Bearer ";
    public HttpInterceptor() {

    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request requisicao=chain.request();
        Request.Builder construtorRequisicao=requisicao.newBuilder();
        construtorRequisicao.addHeader("Accept","application/json");
        Token token = getToken();
        if(token != null) {
            if (token.getAccess_token() != null) {
                construtorRequisicao.addHeader(HEADER_AUTORIZACAO, TIPO_TOKEN + token.getAccess_token());
            }
        }
        Request novaRequisicao=construtorRequisicao.build();
        return chain.proceed(novaRequisicao);
    }

    private Token getToken(){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Token> token;
        token = realm.where(Token.class).equalTo("identificar", 2).findAll();
        return token.size() == 0 ? null : token.first();
    }

}
