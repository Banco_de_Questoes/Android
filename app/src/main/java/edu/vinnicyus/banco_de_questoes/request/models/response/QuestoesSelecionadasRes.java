package edu.vinnicyus.banco_de_questoes.request.models.response;

import android.os.Parcel;
import android.os.Parcelable;

public class QuestoesSelecionadasRes implements Parcelable {

    private int id;
    private String enunciado;
    private String[] alternativas;
    private int alternativa_correta;
    private int nivel;
    private SubCategoriaRes sub_categoria;
    private int professor_id;

    public QuestoesSelecionadasRes(int id, String enunciado, String[] alternativas, int alternativa_correta, int nivel, SubCategoriaRes sub_categoria, int professor_id) {
        this.id = id;
        this.enunciado = enunciado;
        this.alternativas = alternativas;
        this.alternativa_correta = alternativa_correta;
        this.nivel = nivel;
        this.sub_categoria = sub_categoria;
        this.professor_id = professor_id;
    }

    public int getId() {
        return id;
    }

    public String getEnunciado() {
        return enunciado;
    }

    public String[] getAlternativas() {
        return alternativas;
    }

    public int getAlternativa_correta() {
        return alternativa_correta;
    }

    public int getNivel() {
        return nivel;
    }

    public SubCategoriaRes getSub_categoria() {
        return sub_categoria;
    }

    public int getProfessor_id() {
        return professor_id;
    }


    public class Errors{

    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.enunciado);
        dest.writeStringArray(this.alternativas);
        dest.writeInt(this.alternativa_correta);
        dest.writeInt(this.nivel);
        dest.writeParcelable(this.sub_categoria, flags);
        dest.writeInt(this.professor_id);
    }

    protected QuestoesSelecionadasRes(Parcel in) {
        this.id = in.readInt();
        this.enunciado = in.readString();
        this.alternativas = in.createStringArray();
        this.alternativa_correta = in.readInt();
        this.nivel = in.readInt();
        this.sub_categoria = in.readParcelable(SubCategoriaRes.class.getClassLoader());
        this.professor_id = in.readInt();
    }

    public static final Creator<QuestoesSelecionadasRes> CREATOR = new Creator<QuestoesSelecionadasRes>() {
        @Override
        public QuestoesSelecionadasRes createFromParcel(Parcel source) {
            return new QuestoesSelecionadasRes(source);
        }

        @Override
        public QuestoesSelecionadasRes[] newArray(int size) {
            return new QuestoesSelecionadasRes[size];
        }
    };
}
