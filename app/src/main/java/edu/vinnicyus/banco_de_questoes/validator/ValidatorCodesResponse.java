package edu.vinnicyus.banco_de_questoes.validator;

import android.widget.Toast;

import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.lang.reflect.ParameterizedType;

import edu.vinnicyus.banco_de_questoes.request.HttpRefreshToken;
import edu.vinnicyus.banco_de_questoes.request.models.response.ErrorsResponseRes;

public class ValidatorCodesResponse<T,E> {

    private retrofit2.Response<T> response;
    private T value;
    private ErrorsResponseRes<E> errors;
    private int status;
    private Class<?> dataerror;

    public ValidatorCodesResponse(retrofit2.Response<T> r, final Class<?> dataError){
        this.response = r;
        dataerror = dataError;
        status = 2;
    }

    public void execute(){
        if(response != null) {
            switch ((int) response.code()) {
                case 200:
                    value = response.body();
                    this.status = 1;
                    break;
                case 422:
                    Gson gson = new Gson();
                    errors = gson.fromJson(response.errorBody().charStream(), getType(ErrorsResponseRes.class, dataerror));
                    this.status = 0;
                    break;
                default:
                    this.status = 2;
                    break;
            }
        }
    }

    public T getValue() {
        return value;
    }

    public ErrorsResponseRes<E> getErrors() {
        return errors;
    }

    public int isStatus() {
        return status;
    }

    public Type getType(final Class<?> rawClass,final Class<?> parameter) {
        return new ParameterizedType() {
            @Override
            public Type[] getActualTypeArguments() {
                return new Type[] {parameter};
            }
            @Override
            public Type getRawType() {
                return rawClass;
            }
            @Override
            public Type getOwnerType() {
                return null;
            }
        };
    }
}
